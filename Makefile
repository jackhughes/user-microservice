.PHONY: build

build: 
	docker build -t user-microservice -f build/Dockerfile .

skaffold:
	skaffold dev

swagger:
	go generate ./...
	