[![Go Report Card](https://goreportcard.com/badge/gitlab.com/jackhughes/user-microservice)](https://goreportcard.com/report/gitlab.com/jackhughes/user-microservice)

# User Microservice

A small demo application for a user microservice than can be deployed to Kubernetes and Minikube.

## Installation Prerequisites:
  - [Go v1.11.2](https://golang.org/)
  - [Docker](https://docs.docker.com/install/)
  - [Minikube v0.30.0](https://github.com/kubernetes/minikube)
    - Follow all steps [here](https://kubernetes.io/docs/tasks/tools/install-minikube/).
  - [Skaffold](https://skaffold.dev/docs/getting-started/#installing-skaffold)
  - [Go Swagger](https://github.com/go-swagger/go-swagger) 

## Running the application
  - Change directory to your Go working directory and clone this repository
  - Run `minikube start --vm-driver {your_driver}`
  - Allow minikube to install it's dependencies
  - When this is finished, run `make skaffold` from the root of the repository, this will download the applications dependencies using Go Modules (both minikube and this step may take some time on first run).
  - Get the IP address the service by running `minikube service user-service`. This will launch a web browser with the URL and port being the endpoint we need to hit using the API collections linked below.
  - The API may restart several times whilst the MySQL container comes up - wait until you see 200s returned from the APIs healthchecks (`/ready`) before proceeding.
  - Use the IP above in your postman/insomnia collections and try out the application.

## Running Swagger (endpoint documentation)
 - Run `go generate ./...` or `make swagger` from the root of the repository. 

## Insomnia/Postman Collection
  - [Insomnia](https://gitlab.com/jackhughes/user-microservice/snippets/1834078)
  - [Postman](https://gitlab.com/jackhughes/user-microservice/snippets/1834346)

## To Do:
  - Unit Tests :disappointed_relieved:
  - Better self documenting API endpoints (models are currently not working)
  - Abstract the database from the cluster into RDS or another DB platform.

## But why?
  - Kubernetes is the perfect tool for orchestrating docker containers for high availability and resiliency.
  - The database exists in the cluster in order to provide portability - this is not a long term solution and will be addressed in future versions.
  - I use the Docker Builder Pattern to keep image size to an absolute minimum.
  - I use Swagger as it's widely used and accepted (though I haven't managed to fully implement it yet :sob:)
  - I use Skaffold in order to make changes and see those changes reflected in my cluster almost immediately.

## Questions?
Email me at info@jackhugh.es
