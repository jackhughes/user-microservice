//go:generate swagger generate spec -o ../../swagger.yml

// Package main User Microservice API.
//
// A small demo micro service running Docker containers on Minikube
//
//     Schemes: http
//     Host: MinikubeServiceIP
//     BasePath: /
//     Version: 0.0.1
//     Contact: Jack Hughes <info@jackhugh.es>
//
//     Consumes:
//     - application/json
//
// swagger:meta
package main

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/jackhughes/user-microservice/cmd/api/server/gin"
	"gitlab.com/jackhughes/user-microservice/cmd/api/storage"
	"gitlab.com/jackhughes/user-microservice/cmd/api/storage/mysql"
)

// Main loads the Gin config and runs the server
func main() {
	logrus.Info("starting the user-microservice...")
	scfg, err := NewGinConfig()
	if err != nil {
		logrus.Fatalf("could not load config: %v", err)
	}

	srv := gin.NewServer(scfg, setupStorage())
	if err := srv.Run(); err != nil {
		logrus.Fatalf("could not load server: %v", err)
	}
}

// Set up storage and eventually pass the database connection to Gin
func setupStorage() storage.Storage {
	mcfg, err := NewMysqlConfig()
	if err != nil {
		logrus.Fatalf("could not load mysql configuration: %v", err)
	}

	db, err := mysql.NewStorage(mcfg)
	if err != nil {
		logrus.Fatalf("could not connect to mysql: %v", err)
	}

	return db
}
