package gin

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/jackhughes/user-microservice/cmd/api/server"
	validator "gopkg.in/go-playground/validator.v9"
)

// UpdateHandler updates existing users
func (s Server) UpdateHandler(c *gin.Context) {
	uid, err := strconv.Atoi(c.Param("userId"))
	if err != nil {
		logrus.Errorf("user id should be an integer: %v", err)
		c.JSON(http.StatusBadRequest, err)

		return
	}

	usr, err := s.db.GetUser(uid)
	if err != nil {
		logrus.Errorf("failed to find user id: %v: %v", uid, err)
		c.JSON(http.StatusNotFound, err)

		return
	}

	if err := c.BindJSON(usr); err != nil {
		logrus.Errorf("could not bind json to user structure: %v", err)
		c.JSON(http.StatusBadRequest, err)

		return
	}

	if err := validator.New().Struct(usr); err != nil {
		ve := err.(validator.ValidationErrors)
		logrus.Errorf("validation error: %v", ve)
		c.JSON(http.StatusBadRequest, server.ValidationErrors(ve))

		return
	}

	if err := s.db.Update(usr); err != nil {
		logrus.Errorf("could not save model: %v", err)
		c.JSON(http.StatusInternalServerError, err)

		return
	}

	c.JSON(http.StatusOK, nil)
}
