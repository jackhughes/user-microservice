package gin

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// DeleteHandler soft delete users
func (s Server) DeleteHandler(c *gin.Context) {
	uid, err := strconv.Atoi(c.Param("userId"))
	if err != nil {
		logrus.Errorf("user id should be an integer: %v", err)
		c.JSON(http.StatusBadRequest, err)

		return
	}

	_, err = s.db.GetUser(uid)
	if err != nil {
		logrus.Errorf("failed to find user id: %v: %v", uid, err)
		c.JSON(http.StatusNotFound, err)

		return
	}

	if err := s.db.Delete(uid); err != nil {
		logrus.Errorf("failed to delete user id: %v: %v", uid, err)
		c.JSON(http.StatusInternalServerError, err)

		return
	}

	c.JSON(http.StatusOK, nil)
}
