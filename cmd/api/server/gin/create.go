package gin

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/jackhughes/user-microservice/cmd/api/server"
	validator "gopkg.in/go-playground/validator.v9"
)

// CreationHandler creates new users
func (s Server) CreationHandler(c *gin.Context) {
	var usr server.User

	if err := c.BindJSON(&usr); err != nil {
		logrus.Errorf("could not bind json to user structure: %v", err)
		c.JSON(http.StatusBadRequest, err)

		return
	}

	if err := validator.New().Struct(&usr); err != nil {
		ve := err.(validator.ValidationErrors)
		logrus.Errorf("validation error: %v", ve)
		c.JSON(http.StatusBadRequest, server.ValidationErrors(ve))

		return
	}

	if err := s.db.Create(&usr); err != nil {
		logrus.Errorf("failed to persist user: %v", err)
		c.JSON(http.StatusInternalServerError, err)

		return
	}

	c.JSON(http.StatusOK, nil)
}
