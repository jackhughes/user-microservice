package gin

import (
	"net"

	"github.com/gin-gonic/gin"
	"gitlab.com/jackhughes/user-microservice/cmd/api/storage"
)

// Config currently only stores the port
// but could contain other things beneficial to the server
type Config struct {
	Port string
}

// Server holds the Config (thus containing the port)
// and the Gin Engine the server runs
type Server struct {
	cfg *Config
	gin *gin.Engine

	db storage.Storage
}

// NewServer instantiates the server with the config loaded in main.go
// It also sets up router groups and paths to actions
func NewServer(cfg *Config, storage storage.Storage) *Server {
	r := gin.Default()
	gin.SetMode(gin.ReleaseMode)

	s := &Server{cfg: cfg, gin: r, db: storage}

	api := r.Group("/")
	// swagger:route GET /ready ready ready
	//
	// Basic ready check by pinging the database connection
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: http
	//
	//     Responses:
	//       default: genericError
	//       200: alive
	//       500: failed
	api.GET("/ready", s.ReadyHandler)

	// swagger:route POST /create user create
	//
	// Creates a user
	//
	//     Consumes:
	//     - application/json
	//     Produces:
	//     - application/json
	//
	//     Schemes: http
	//
	//     Responses:
	//       default: genericError
	//       200: created
	//       400: bad_request
	//       500: failed
	api.POST("/create", s.CreationHandler)

	// swagger:route PATCH /update user update
	//
	// Updates a user
	//
	//     Consumes:
	//     - application/json
	//     Produces:
	//     - application/json
	//
	//     Schemes: http
	//
	//     Responses:
	//       default: genericError
	//       200: updated
	//       400: bad_request
	//       500: failed
	api.PATCH("/update/:userId", s.UpdateHandler)

	// swagger:route DELETE /delete user delete
	//
	// Deletes a user
	//
	//     Consumes:
	//     - application/json
	//     Produces:
	//     - application/json
	//
	//     Schemes: http
	//
	//     Responses:
	//       default: genericError
	//       200: deleted
	//       400: bad_request
	//       500: failed
	api.DELETE("/delete/:userId", s.DeleteHandler)

	// swagger:route POST /create user create
	//
	// List users
	//
	//     Consumes:
	//     - application/x-www-form-urlencoded
	//     Produces:
	//     - application/json
	//
	//     Schemes: http
	//
	//     Responses:
	//       default: genericError
	//       200: ok
	//       400: bad_request
	//       500: failed
	api.GET("/", s.ListHandler)

	return s
}

// Run the server, if this fails, return an error
func (s Server) Run() error {
	return s.gin.Run(net.JoinHostPort("", s.cfg.Port))
}
