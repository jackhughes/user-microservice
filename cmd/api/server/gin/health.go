package gin

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// ReadyHandler basic db ping check
func (s Server) ReadyHandler(c *gin.Context) {
	if err := s.db.Ping(); err != nil {
		c.JSON(http.StatusInternalServerError, err)

		return
	}

	c.JSON(http.StatusOK, nil)
}
