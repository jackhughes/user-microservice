package gin

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/jackhughes/user-microservice/cmd/api/server"
	validator "gopkg.in/go-playground/validator.v9"
)

// ListHandler creates new users
func (s Server) ListHandler(c *gin.Context) {
	var ls server.Limit
	c.Bind(&ls)

	if err := validator.New().Struct(&ls); err != nil {
		ve := err.(validator.ValidationErrors)
		logrus.Errorf("validation error: %v", ve)
		c.JSON(http.StatusBadRequest, server.ValidationErrors(ve))

		return
	}

	usrs, err := s.db.List(ls.CountryCode, *ls.Offset, *ls.Limit)
	if err != nil {
		logrus.Errorf("failed to get users: %v", err)
		c.JSON(http.StatusInternalServerError, err)

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"offset": ls.Offset,
		"limit":  ls.Limit,
		"users":  usrs,
	})
}
