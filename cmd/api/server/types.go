package server

import (
	"time"

	validator "gopkg.in/go-playground/validator.v9"
)

// User the user model with struct tags for JSON decoding, validation rules and GORM column names
// swagger:model
type User struct {
	// The id for the user
	//
	// required: true
	// min: 1
	ID uint `gorm:"primary_key;column:Id"`
	// The first name for the user
	//
	// required: true
	// min: 1
	// max: 50
	FirstName string `json:"firstName" validate:"required,max=50" gorm:"column:FirstName"`
	// The last name for the user
	//
	// required: true
	// min: 1
	// max: 50
	LastName string `json:"lastName" validate:"required,max=50" gorm:"column:LastName"`
	// The nickname for the user
	//
	// required: true
	// min: 1
	// max: 100
	Nickname string `json:"nickname" validate:"required,max=100" gorm:"column:Nickname"`
	// The email address of the user
	//
	// required: true
	// example: user@faceit.com
	Email string `json:"email" validate:"required,email" gorm:"column:Email"`
	// The country code of the user
	//
	// required: true
	// min:2
	// max:2
	// example: GB
	CountryCode string `json:"countryCode" validate:"required,len=2" gorm:"column:CountryCode"`
	// The created at time
	CreatedAt *time.Time `gorm:"column:CreatedAt"`
	// The updated at time
	UpdatedAt *time.Time `gorm:"column:UpdatedAt"`
	// The deleted at time
	DeletedAt *time.Time `gorm:"column:DeletedAt"`
}

// Limit is a struct for the query params required for the List endpoint
// swagger:model
type Limit struct {
	// The offset
	//
	// required: true
	Offset *int `form:"offset" validate:"required,numeric"`
	// The limit
	//
	// required: true
	Limit *int `form:"limit" validate:"required,numeric"`
	// The country code of the user
	CountryCode string `form:"country" validate:"omitempty,len=2"`
}

// TableName overwrites the default table name
func (User) TableName() string {
	return "User"
}

// ValidationErrors formats errors to be appropriate for a JSON response body
func ValidationErrors(e error) []map[string]string {
	ve := e.(validator.ValidationErrors)
	ifi := make([]map[string]string, 0)

	for _, e := range ve {
		errors := map[string]string{}
		errors[e.Field()] = e.Tag()
		ifi = append(ifi, errors)
	}

	return ifi
}
