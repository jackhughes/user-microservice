package mysql

import (
	"fmt"
	"net"

	"gitlab.com/jackhughes/user-microservice/cmd/api/server"

	"github.com/jinzhu/gorm"
	// Import the mysql driver
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/sirupsen/logrus"
	"gitlab.com/jackhughes/user-microservice/cmd/api/storage"
)

// Config contains configuration values used by mysql.
type Config struct {
	Username string
	Password string
	Host     string
	Port     string
	Database string
}

// Storage is an implementation of the generic interface storage.Storage for MySQL
type Storage struct {
	mdb *gorm.DB
}

// NewStorage accepts a Config and returns a pointer to a new Storage.
func NewStorage(cfg *Config) (storage.Storage, error) {
	cs := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		cfg.Username,
		cfg.Password,
		net.JoinHostPort(cfg.Host, cfg.Port),
		cfg.Database,
	)

	db, err := gorm.Open("mysql", cs)
	if err != nil {
		logrus.Errorf("could not connect to the database: %v", err)
	}

	return &Storage{mdb: db}, nil
}

// Create a user based on a hydrated user model. Return an error on failure
func (s *Storage) Create(usr *server.User) error {
	logrus.Infof("attempting to create user: %v", usr.Email)
	return s.mdb.Create(usr).Error
}

// GetUser based on their user id and return a model or error
func (s *Storage) GetUser(uid int) (*server.User, error) {
	logrus.Infof("attempting to retrieve user id: %v", uid)
	var usr server.User

	if err := s.mdb.First(&usr, uid).Error; err != nil {
		return &server.User{}, err
	}

	return &usr, nil
}

// Update a user model with its new set properties
func (s *Storage) Update(usr *server.User) error {
	logrus.Infof("attempting to update user id: %v", usr.ID)

	return s.mdb.Model(&usr).Updates(usr).Error
}

// Delete soft deletes a user by setting the DeletedAt timestamp to now
func (s *Storage) Delete(uid int) error {
	logrus.Infof("attempting to soft delete user id: %v", uid)
	var usr server.User

	return s.mdb.Delete(&usr, uid).Error
}

// List the users with the optional field country - if this isn't provided all users will be returned
func (s *Storage) List(ctry string, off, lim int) (*[]server.User, error) {
	logrus.Infof("attempting to retrieve paginated results")
	var usrs []server.User

	if len(ctry) == 0 {
		if err := s.mdb.Offset(off).Limit(lim).Find(&usrs).Error; err != nil {
			return nil, err
		}
	} else {
		if err := s.mdb.Offset(off).Limit(lim).Find(&usrs, "CountryCode = ?", ctry).Error; err != nil {
			return nil, err
		}
	}

	return &usrs, nil
}

// Ping the database connection for a readiness check
func (s *Storage) Ping() error {
	return s.mdb.DB().Ping()
}
