package storage

import "gitlab.com/jackhughes/user-microservice/cmd/api/server"

// Storage is the default generic interface for the API
type Storage interface {
	// Create a new user, pass the model and return an error on failure
	Create(user *server.User) error

	// GetUser gets a user model from the database based on the user id
	GetUser(uid int) (*server.User, error)

	// Update a user based on the changed model
	Update(user *server.User) error

	// Delete soft delete the user id
	Delete(uid int) error

	// List all users with the optional field ctry
	List(ctry string, off int, limit int) (*[]server.User, error)

	// Pings the database for a readiness check
	Ping() error
}
