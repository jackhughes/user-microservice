package main

import (
	"github.com/kelseyhightower/envconfig"

	"gitlab.com/jackhughes/user-microservice/cmd/api/server/gin"
	"gitlab.com/jackhughes/user-microservice/cmd/api/storage/mysql"
)

// NewGinConfig load the Gin config from the environment
func NewGinConfig() (*gin.Config, error) {
	cfg := &gin.Config{}
	err := envconfig.Process("server", cfg)
	return cfg, err
}

// NewMysqlConfig load the mysql config from the environment
func NewMysqlConfig() (*mysql.Config, error) {
	cfg := &mysql.Config{}
	err := envconfig.Process("mysql", cfg)
	return cfg, err
}
