module gitlab.com/jackhughes/user-microservice

require (
	cloud.google.com/go v0.36.0 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20190204142019-df6d76eb9289 // indirect
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/go-openapi/analysis v0.18.0 // indirect
	github.com/go-openapi/errors v0.18.0 // indirect
	github.com/go-openapi/inflect v0.18.0 // indirect
	github.com/go-openapi/loads v0.18.0 // indirect
	github.com/go-openapi/runtime v0.18.0 // indirect
	github.com/go-openapi/spec v0.18.0 // indirect
	github.com/go-openapi/strfmt v0.18.0 // indirect
	github.com/go-openapi/swag v0.18.0 // indirect
	github.com/go-openapi/validate v0.18.0 // indirect
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/go-swagger/go-swagger v0.18.0 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/golang/protobuf v1.3.0 // indirect
	github.com/gorilla/handlers v1.4.0 // indirect
	github.com/jessevdk/go-flags v1.4.0 // indirect
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/jinzhu/now v1.0.0 // indirect
	github.com/json-iterator/go v1.1.5 // indirect
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/lib/pq v1.0.0 // indirect
	github.com/mattn/go-isatty v0.0.6 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/viper v1.3.1 // indirect
	github.com/toqueteos/webbrowser v1.1.0 // indirect
	github.com/ugorji/go/codec v0.0.0-20190204201341-e444a5086c43 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/go-playground/validator.v9 v9.27.0
)
