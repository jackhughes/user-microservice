CREATE DATABASE IF NOT EXISTS Faceit;
USE Faceit;
CREATE TABLE IF NOT EXISTS `User` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `Nickname` varchar(100) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `CountryCode` char(2) NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DeletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
INSERT IGNORE INTO `User` VALUES (1,'Jack','Hughes','TheLegend27','info@jackhugh.es','GB','2019-03-10 12:38:35','2019-03-10 12:38:35',NULL),(2,'Face','It','Master921','face@it.com','SD','2019-03-10 12:38:35','2019-03-10 12:38:35',NULL),(3,'John','Smith','JohnnyBoy99','johnsmith@gmail.com','TG','2019-03-10 12:38:35','2019-03-10 12:38:35',NULL),(4,'Bill','Gates','Gatesy336','bill@microsoft.com','US','2019-03-10 12:38:35','2019-03-10 12:38:35',NULL),(5,'Carl','Invoker','grandmagus2','gabe.newell@valvesoftware.com','US','2019-03-10 12:38:35','2019-03-10 12:38:35',NULL);